# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mongoid_sequences/version'

Gem::Specification.new do |spec|
  spec.name          = "mongoid_sequences"
  spec.version       = MongoidSequences::VERSION
  spec.authors       = ["nii_kenichi"]
  spec.email         = ["kenichi@guucy.com"]

  spec.summary       = %q{Specify fields to behave like a sequence number.}
  spec.description   = %q{Mongoid::Sequences gives you the ability to specify field to behave like a sequence number.}
  spec.homepage      = 'https://bitbucket.org/guucy/mongoid_sequences'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.executables   = `git ls-files -- bin/*`.split('\n').map { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency "mongoid", "~> 4.0", "< 5"
  spec.add_dependency "activesupport", ">= 3.2", "< 5"

  spec.add_development_dependency "rspec"
end
