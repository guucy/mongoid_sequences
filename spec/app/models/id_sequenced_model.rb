class IdSequencedModel
  include Mongoid::Document
  include Mongoid::Sequences

  sequence :_id
end