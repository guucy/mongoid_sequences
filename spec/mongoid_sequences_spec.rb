require 'spec_helper'

describe MongoidSequences do
  it 'has a version number' do
    expect(MongoidSequences::VERSION).not_to be nil
  end

  it 'does generated new record id will be not null' do
    expect(IdSequencedModel.new.id).not_to be nil
  end

  it 'does generated string type' do
    m1 = IdSequencedModel.create

    expect(m1.id).to eq '1'
  end

  it 'does generated ids was incremented and no repeat' do
    m1 = IdSequencedModel.create
    m2 = IdSequencedModel.create
    m3 = IdSequencedModel.create
    expect(m2.id.to_i - 1).to eq m1.id.to_i
    expect(m3.id.to_i - 2).to eq m1.id.to_i
    m3.destroy

    m4 = IdSequencedModel.create
    expect(m4.id.to_i - 3).to eq m1.id.to_i
  end

end
