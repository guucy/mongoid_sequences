# MongoidSequences

Mongoid Sequences allow you to specify fields to behave like a sequence number.

## Status

[![wercker status](https://app.wercker.com/status/e2c38d496db1c64b1eccd8795c373b4c/m "wercker status")](https://app.wercker.com/project/bykey/e2c38d496db1c64b1eccd8795c373b4c)

## Credits

This gem was copy from https://github.com/Johaned/mongoid-sequence

## Installation

Add this line to your application's Gemfile:

```ruby
git_source(:bitbucket) do |repo_name|
  "https://bitbucket.org/#{repo_name}.git"
end

gem 'mongoid_sequences', bitbucket: 'guucy/mongoid_sequences'
```

## Usage

Include `Mongoid::Sequences` in your class and call `sequence(:field)`.

Like this:

```ruby
class Sequenced
    include Mongoid::Document
    include Mongoid::Sequences

    field :my_sequence, :type => Integer
    sequence :my_sequence
end

s1 = Sequenced.create
s1.sequence #=> '1'

s2 = Sequenced.create
s2.sequence #=> '2' # and so on
```

It's also possible to make the `id` field behave like this:

```ruby
class Sequenced
    include Mongoid::Document
    include Mongoid::Sequences

    sequence :_id
end

s1 = Sequenced.create
s1.id #=> '1'

s2 = Sequenced.create
s2.id #=> '2' # and so on
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request